<?php

use Latte\Runtime as LR;

/** source: template/template.latte */
final class Template587c589a54 extends Latte\Runtime\Template
{
	public const Source = 'template/template.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Test</title>
  </head>
  <style></style>
  <body>
    <h1>Průměry známek ročníků na střední</h1>
    <table>
      <thead>
        <tr>
          <th></th>
          <th>první pololetí</th>
          <th>druhé pololetí</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>první ročník</th>
          <td>';
		echo LR\Filters::escapeHtmlText($prumer1_1) /* line 22 */;
		echo '</td>
          <td>';
		echo LR\Filters::escapeHtmlText($prumer1_2) /* line 23 */;
		echo '</td>
        </tr>
        <tr>
          <th>druhý ročník</th>
          <td>';
		echo LR\Filters::escapeHtmlText($prumer2_1) /* line 27 */;
		echo '</td>
          <td>';
		echo LR\Filters::escapeHtmlText($prumer2_2) /* line 28 */;
		echo '</td>
        </tr>
        <tr>
          <th>třetí ročník</th>
          <td>';
		echo LR\Filters::escapeHtmlText($prumer3_1) /* line 32 */;
		echo '</td>
          <td>';
		echo LR\Filters::escapeHtmlText($prumer3_2) /* line 33 */;
		echo '</td>
        </tr>
        <tr>
          <th>čtvrtý ročník</th>
          <td>';
		echo LR\Filters::escapeHtmlText($prumer4_1) /* line 37 */;
		echo '</td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
';
	}
}
