<?php
require_once("vendor/autoload.php");
$latte = new Latte\Engine;

$latte->setTempDirectory('temp');

$vysledky = [
    'prumer1_1' => prumer(1), // první ročník, první pololetí
    'prumer1_2' => prumer(2), // druhý ročník, druhé pololetí
    'prumer2_1' => prumer(3), // ...
    'prumer2_2' => prumer(4),
    'prumer3_1' => prumer(5),
    'prumer3_2' => prumer(6),
    'prumer4_1' => prumer(7),
];

function prumer($pololeti){

    $file = fopen('stats.csv', 'r');
    $array = [];

    while (($data = fgetcsv($file,1000,",")) !== false) {
        $array[] = $data;
    }
    $pocet_zaku = count($array);
    $soucet = 0;

    for($i = 1; $i < count($array); $i++){
        $prvek = $array[$i] [$pololeti];
        $soucet += $prvek;
    }
    fclose($file);
    return $soucet / $pocet_zaku;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Results</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background: url('background.jpg') no-repeat center center fixed;
            background-size: cover;
            margin: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            position: relative;
        }

        /* Black and white overlay */
        body::after {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5); /* Adjust the alpha value for opacity */
        }

        .container {
            width: 80%;
            max-width: 1200px;
            padding: 20px;
            background-color: rgba(255, 255, 255, 0.8);
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: center;
            position: relative;
            z-index: 1; /* Ensure the container stays on top of the black overlay */
        }

        h1 {
            font-size: 28px;
            margin-bottom: 20px;
        }

        .result {
            border: 1px solid #ccc;
            border-radius: 5px;
            padding: 10px;
            margin-bottom: 10px;
            background-color: #f9f9f9;
        }

        .label {
            font-weight: bold;
            color: #333;
        }

        .value {
            margin-left: 10px;
            color: #666;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Results</h1>
        <?php foreach ($vysledky as $key => $value): ?>
            <div class="result">
                <span class="label"><?= $key ?>:</span>
                <span class="value"><?= $value ?></span>
            </div>
        <?php endforeach; ?>
    </div>
</body>
</html>
